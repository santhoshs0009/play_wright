import { test, expect } from '@playwright/test';

test.use({
  viewport: {
    height: 600,
    width: 800
  }
});

test('test', async ({ page }) => {
  await page.goto('https://uat.cloud.guhroo.co/');
  await page.goto('https://uat.cloud.guhroo.co/login');
  await page.getByPlaceholder('Enter username').click();
  await page.getByPlaceholder('Enter username').fill('devi');
  await page.getByPlaceholder('Enter password').click();
  await page.getByPlaceholder('Enter password').fill('pass');
  await page.getByRole('button', { name: 'Sign in' }).click();
  await page.getByRole('link', { name: ' Invite Client' }).click();
  await page.getByRole('link', { name: ' Invite Reseller' }).click();
});